﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour {

    private float x_axis, y_axis = 0, z_axis, t;
    public float velocidad;


    void moverse()
    {
        if (this.transform.position.z < 1 && this.transform.position.z > -1)
            x_axis = Input.GetAxis("Horizontal");
        else
        {
            x_axis = 0;
            if(this.transform.position.x > 1 || this.transform.position.x < -1)
                transform.Translate(0f, 0f, (this.transform.position.z) *(-1));
        }

        if (this.transform.position.x < 1 && this.transform.position.x > -1)
            z_axis = Input.GetAxis("Vertical");
        else {

            z_axis = 0;
            if (this.transform.position.z > 1 || this.transform.position.z < -1)
                transform.Translate((this.transform.position.x) * (-1), 0f, 0f);
        }

        if (this.transform.position.x < 1 && this.transform.position.x > -1)
            z_axis = Input.GetAxis("Vertical");
        else
        {

            z_axis = 0;
            if (this.transform.position.z > 1 || this.transform.position.z < -1)
                transform.Translate((this.transform.position.x) * (-1), 0f, 0f);
        }

        if (Input.GetKeyDown(KeyCode.Z))
            y_axis = 1;
        if (Input.GetKeyUp(KeyCode.Z))
            y_axis = 0;
        if (Input.GetKeyDown(KeyCode.X))
            y_axis = -1;
        if (Input.GetKeyUp(KeyCode.X))
            y_axis = 0;
    }

	// Use this for initialization
	void Start () {
        t = Time.deltaTime;
	}
	
	// Update is called once per frame
	void Update () {

        moverse();

        transform.Translate(x_axis*t*velocidad, y_axis*t*velocidad, z_axis*t*velocidad);
    }
}
