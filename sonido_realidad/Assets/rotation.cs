﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotation : MonoBehaviour {

    private float x_axis = 0, t;
    public float velocidad;

    void rotar(bool n, bool nn, bool m, bool mm)
    {
        if (n)
            x_axis = 1;
        if (nn)
            x_axis = 0;
        if (m)
            x_axis = -1;
        if (mm)
            x_axis = 0;
    }

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        t = Time.deltaTime;

        rotar(Input.GetKeyDown(KeyCode.C), Input.GetKeyUp(KeyCode.C), Input.GetKeyDown(KeyCode.LeftShift), Input.GetKeyUp(KeyCode.LeftShift));

        transform.Rotate(0f, x_axis * t * velocidad, 0f);
	}
}
